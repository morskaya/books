from django.conf.urls import url
from critic import views

urlpatterns = [
    url(r'^review/', views.ReviewView.as_view(), 
    	name='review_book'), 
    url(r'^thanks/', views.ThanksView.as_view()),
]