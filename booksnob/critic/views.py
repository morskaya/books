from critic.forms import ReviewForm
from django.views.generic import TemplateView
from django.views.generic.edit import FormView

class ReviewView(FormView):
    template_name = 'critic/review.html'
    form_class = ReviewForm
    success_url = '/critic/thanks/'

    def form_valid(self, form):
        form.send_email()
        return super(ReviewView, self).form_valid(form)

class ThanksView(TemplateView):
	template_name = 'critic/thanks.html'    
