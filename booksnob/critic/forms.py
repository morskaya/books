from django import forms

from reader.actions import get_books


class ReviewForm(forms.Form):
    books = forms.ChoiceField(
        choices=[(book, book) for book in get_books()]
    )
    text = forms.CharField(widget=forms.Textarea)

    def send_email(self):
        print ('Book: {0}'.format(self.cleaned_data['books']))
        print ('Message: {0}'.format(self.cleaned_data['text']))
