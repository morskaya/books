# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reader', '0002_readers'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='readers',
            name='book',
        ),
        migrations.AddField(
            model_name='readers',
            name='book',
            field=models.ManyToManyField(to='reader.Book'),
        ),
    ]
