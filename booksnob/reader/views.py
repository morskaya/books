from django.views.generic import TemplateView, View
from reader.models import Author, Book
from reader.actions import get_books, get_authors
from reader.forms import AuthorForm
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic.edit import FormView, UpdateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
import pytz
from django.utils import timezone
#from django.core.cache import get_cache



class Home(TemplateView):
	template_name = 'reader/home.html'

class HomeView(TemplateView):
	template_name = 'reader/add_author.html'

	def get_context_data(self, **kwargs):
		context = super(HomeView, self).get_context_data(**kwargs)
		context['authors'] = get_authors()
		context['books'] = get_books()
		return context

def redirected(request):
	return HttpResponseRedirect('/home/')

def get_author_filter(request):
	#Author.filter_author.get_author()
	#if request.POST:
	form = AuthorForm()
	return HttpResponse(form)


class AuthorViews(FormView):
	template_name = 'reader/add_author.html'
	success_url = '/add_author/'
	form_class = AuthorForm

	def form_valid(self, form):
		self.object = form.save()
		super(AuthorViews, self).form_valid(form)
		return HttpResponseRedirect('/author_list/')


class GetAuthor(DetailView):
	model = Author
	template_name = 'reader/detail.html'

	def get_context_data(self, **kwargs):
		context = super(GetAuthor, self).get_context_data(**kwargs)
		context['books'] = get_books()
		return context


class GetAuthorList(ListView):
	model = Author
	template_name = 'reader/list.html'

	def post(self, request, *args, **kwargs):
		context = request.POST.get('text', '')
	 	author = Author.objects.filter(first_name__icontains=context)
		return render(request, 'reader/list.html', {'author_list': author})

class Register(FormView):
	template_name = 'reader/registr.html'
	success_url = '/registr/'
	form_class = UserCreationForm

	def form_valid(self, form):
		self.object = form.save()
		super(Register, self).form_valid(form)
		return HttpResponseRedirect('/author_list/')

class Avtorization(FormView):
	template_name = 'reader/avtoriz.html'
	success_url = '/avtoriz/'
	form_class = AuthenticationForm

	def form_valid(self, form):
		user = form.get_user()
		login(self.request, user)
		super(Avtorization, self).form_valid(form)
		return HttpResponseRedirect('/author_list/')

class LogOut(View):
	def get(self, request, *args, **kwargs):
		logout(request)
		return HttpResponseRedirect('/home/')

class AuthorUpdate(UpdateView):
	model = Author
	fields = '__all__'
	template_name = 'reader/update_author.html'
	success_url = '/author_list/'


def set_timezone(request):
    if request.method == 'POST':
        request.session['django_timezone'] = request.POST['timezone']
        return redirect('/set_timezone/')
    else:
        return render(request, 'reader/timezone.html', {'timezones': pytz.common_timezones})


	'''
	def orderby(self, request, *args, **kwargs):
		up = request.POST.get('up', '')
		Author.objects.filter(first_name__icontains=up).order_by('fist_name')
		down = request.POST.get('down', '')
		Author.objects.filter(first_name__icontains=down).order_by('fist_name')	
		'''