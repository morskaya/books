from django.core.management.base import BaseCommand, CommandError
from reader.models import Author

class Command(BaseCommand):
	help = 'Help'

	def add_arguments(self, parser):
		parser.add_argument('first_name', nargs='+', type=str)
		parser.add_argument('--delete',
            action='store_true',
            dest='delete',
            default=False,
            help='Delete author instead of closing it')

	def handle(self, *args, **kwargs):
		print kwargs
		author = Author.objects.filter(first_name='first_name')
		if kwargs['delete']:
			author.delete()
