from django import forms
from reader.actions import get_books
from reader.models import Author


class ReviewForm(forms.Form):
    books = forms.ChoiceField(
        choices=[(book, book) for book in get_books()]
    )
    text = forms.CharField(widget=forms.Textarea)

    def send_email(self):
        print ('Book: {0}'.format(self.cleaned_data['books']))
        print ('Message: {0}'.format(self.cleaned_data['text']))

class AuthorForm(forms.ModelForm):
	class Meta:
		model = Author
		fields = '__all__'

