from django.db import models
from django.contrib.auth.models import User

class AuthorManager(models.Manager):
	def get_author(self):
		q = super(AuthorManager, self).get_queryset()
		return q.filter(first_name="Dog")

class Author(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=40)
    email = models.EmailField()
    image = models.ImageField(upload_to='images/avatar', blank=True)
    objects = models.Manager
    filter_author = AuthorManager

    def __unicode__(self):
    	return '{0} {1}'.format(self.first_name, self.last_name)

class Book(models.Model):
	author = models.ForeignKey(Author)
	name = models.CharField(max_length=100)

	def __unicode__(self):
		return '{0} {1} - {2}'.format(self.author.first_name, self.author.last_name, self.name)

class Readers(models.Model):
	first_name = models.CharField(max_length=30)
	last_name = models.CharField(max_length=40)
	book = models.ManyToManyField(Book)

	def __unicode__(self):
		return '{0} {1} - {2}'.format(self.first_name, self.last_name, self.book.name)

