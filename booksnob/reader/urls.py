"""
URL blog Configuration
"""

from django.conf.urls import include, url
from django.contrib import admin
from reader import views
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings


urlpatterns = [
	url(r'^$', views.redirected),
    url(r'^home/$', views.Home.as_view(), name='home'),
    url(r'^add_author/$', views.AuthorViews.as_view(), name='add_author'),
    url(r'^get_author/(?P<pk>\d+)/$', views.GetAuthor.as_view(), name='detail'),
    url(r'^author_list/$', views.GetAuthorList.as_view(), name='author_list'),
    url(r'^registr/$', views.Register.as_view(), name='registr'),
    url(r'^avtoriz/$', views.Avtorization.as_view(), name='avtoriz'),
    url(r'^logout/$', views.LogOut.as_view(), name='logout'),
    url(r'^set_timezone/$', views.set_timezone, name='set_timezone'),
    url(r'^author_update/(?P<pk>\d+)/$', views.AuthorUpdate.as_view(), name="author_update"),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()