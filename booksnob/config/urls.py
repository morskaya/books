from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^critic/', include('critic.urls')),
    url(r'^', include('reader.urls')),
    url(r'^djrestframe/', include('djframework.urls')),
    url(r'^author_list_swagger/$', include('rest_framework_swagger.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
